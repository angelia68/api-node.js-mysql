//Import
const express =  require("express")
const mysql = require("mysql")
const bodyparser = require("body-parser")

//Initialization
let app = express()

//Preparation middleware
app.use(bodyparser.json())

//query = ?
//params = /
//body = POST

//1. Contoh Test, Resultnya Hello World
//URL: http://localhost:8000/test
//RES: Hello World
app.get("/test", function(req, res){
    res.send("Hello World")
})

//2. Contoh Get User tanpa request
//URL: http://localhost:8000/user
//RES: {"userId":"001","username":"Angelia"}
app.get("/user", function(req,res){
    res.send({
        userId : "001",
        username : "Angelia"
    })
})

//3. Contoh Get Specific User dengan params (/)
//URL: http://localhost:8000/user/2
//RES: {"userId":"2","username":"Angelia"}
app.get("/specificuser/:idUser", function(req, res){
    // let userId = req.query.userId
    let userId = req.params.idUser
    res.send({
        userId : userId,
        username : "Angelia"
    })
})

//4. Contoh Get Specific User dengan query (?)
//URL: http://localhost:8000/specificuser?userId=002
//RES: {"userId":"002","username":"Angelia"}
app.get("/specificuser", function(req, res){
    let userId = req.query.userId
    res.send({
        userId : userId,
        username : "Angelia"
    })
})

//5. Contoh Get Account dengan body (POST), testingnya pakai POSTMAN
//URL: http://localhost:8000/getAccount
/*BODY: 
{
    "username" : "Angelia",
    "password" : "123"
} */
//RES: { "account": "Angelia123" }
app.post("/getAccount", function(req,res){
    let username = req.body.username
    let password = req.body.password
    res.send({
        account : username + password
    })
})

//LEARNING CRUD
//--Database Connection preparation
let connection = {
    host : "localhost",
    user : "root",
    password : "",
    database : "testing"
}
//--Connect to MySQL
let client = mysql.createConnection(connection)

//1. CREATE/INSERT
//URL: localhost:8000/insertNewUser
/*BODY: 
{
    "username" : "cynthias",
    "password" : "789",
    "name" : "Cynthia",
    "sex" : "F"
}
*/
/*RES:
{
    "code": "200",
    "message": "",
    "status": "Success"
}
*/
app.post("/insertNewUser", function(req, res){
    client.connect(function(err){
        if(err) res.send({
          code : "500",
          message : err.message,
          data : {}
        })
        else{
            let users = {
                "username" : req.body.username,
                "password" : req.body.password,
                "name" : req.body.name,
                "sex" : req.body.sex
            }
            client.query("INSERT INTO msuser SET ?", users, function(error, result){
                //End connection
                client.end()
                if(error) res.send({
                    code: "400",
                    message : error.sqlMessage,
                    data : {}
                })
                else{
                    res.send({
                        code : "200",
                        message : "",
                        data : {
                            status : "Success"
                        }
                    })
                }
            })
        }
    })
})

//2. READ/GET
//URL: localhost:8000/getUser
//BODY: -
/*RES:
{
    "code": "200",
    "message": "",
    "data": [
        {
            "userId": 1,
            "username": "angelia",
            "password": "123",
            "name": "Angelia",
            "sex": "F"
        },
        {
            "userId": 2,
            "username": "budi",
            "password": "456",
            "name": "Budi",
            "sex": "M"
        }
    ]
}
 */
app.get("/getUser", function(req, res){
    client.connect(function(err){
        if(err) res.send({
          code : "500",
          message : err.message,
          data : {}
        })
        else{
            client.query("SELECT userId, username, password, name, sex FROM msuser", function(error, rows){
                //End connection
                client.end()
                if(error) res.send({
                    code: "400",
                    message : error.sqlMessage,
                    data : {}
                })
                else{
                    res.send({
                        code : "200",
                        message : "",
                        data : rows
                    })
                }
            })
        }
    })
})

//3. UPDATE
//URL: localhost:8000/updatePassword
/*BODY: 
{
    "userId" : "4",
    "password" : "abc"
}
*/
/*RES:
{
    "code": "200",
    "message": "",
    "data": {
        "status": "Password Updated"
    }
}
 */
app.put("/updatePassword", function(req, res){
    client.connect(function(err){
        if(err) res.send({
          code : "500",
          message : err.message,
          data : {}
        })
        else{
            client.query("UPDATE msuser SET password='"+req.body.password+"' WHERE userId='"+req.body.userId+"'", function(error, results){
                //End connection
                client.end()
                if(error) res.send({
                    code: "400",
                    message : error.sqlMessage,
                    data : {}
                })
                else{
                    res.send({
                        code : "200",
                        message : "",
                        data : {
                            status : "Password Updated"
                        }
                    })
                }
            })
        }
    })
})


//4. DELETE
//URL: localhost:8000/deleteUser
//Body: {"userId" : "5"}
/*RES:
{
    "code": "200",
    "message": "",
    "data": {
        "status": "Success Deleting User!"
    }
}
*/
app.delete("/deleteUser", function(req, res){
    client.connect(function(err){
        if(err) res.send({
          code : "500",
          message : err.message,
          data : {}
        })
        else{
            let userId = req.body.userId
            client.query("DELETE FROM msuser WHERE userId='"+userId+"'", function(error, results){
                //End connection
                client.end()
                if(error) res.send({
                    code: "400",
                    message : error.sqlMessage,
                    data : {}
                })
                else{
                    res.send({
                        code : "200",
                        message : "",
                        data : {
                            status : "Success Deleting User!"
                        }
                    })
                }
            })
        }
    })
})


//Listener
app.listen(8000, function(){
    console.log("Listening to Port 8000")
})


//===== GUIDE =====
//Executor
// node route.js

// Unstuck Terminal
//ctrl + c

// Access DB
//http://localhost/phpmyadmin/

/*RESTful API REFERENCES:
http://mfikri.com/en/blog/nodejs-restful-api-mysql
*/
